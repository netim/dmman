Rails.application.routes.draw do
  # Rotas adicionadas pelo Devise e ActiveAdmin
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout'}
  # Recursos para geração de urls
  resources :users
  resources :planned_activities,          only: [:create, :show]
  resources :performed_activities,          only: [:create, :show]
  # Path raiz direciona para a action show do controller de usuários
  root 'users#show'
  # Paths do sistema para exibição e adição das atividades
  get '/planned_activities/', to: 'planned_activities#new'
  get '/performed_activities/', to: 'performed_activities#new'
  get '/planned_activities/:id', to: 'planned_activities#show'
  get '/performed_activities/:id', to: 'performed_activities#show'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
