# Configuração do rufus scheduler para enviar emails com link às 8 horas
# para atividades planejadas e às 16 horas para atividades realizadas
sched = Rufus::Scheduler.singleton

sched.cron('00 08 * * 1-5 America/Fortaleza') do
  User.find_each do |user|
    PlannedMailer.with(user: user).planned_email.deliver_now
  end
end

sched.cron('00 16 * * 1-5 America/Fortaleza')  do
  User.find_each do |user|
    PerformedMailer.with(user: user).performed_email.deliver_now
  end
end
