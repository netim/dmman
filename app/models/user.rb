class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :planned_activities,  class_name: "PlannedActivity", foreign_key: "user_id", dependent: :destroy
  has_many :performed_activities,  class_name: "PerformedActivity", foreign_key: "user_id", dependent: :destroy
end
