class PlannedActivitiesController < ApplicationController
  # Utilização da gem hashids para obfuscar os IDs das atividades dos usuários
  # fonte: https://stackoverflow.com/questions/5496935/hash-instead-of-id

  def new
    @planned_activity = current_user.planned_activities.build
  end

  def create
    @planned_activity = current_user.planned_activities.build(planned_activity_params)
    if @planned_activity.save
      flash[:success] = "Atividades planejadas salvas!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def destroy
  end

  def show
    id = Hashids.new("DMMan",4).decode(params[:id]).try(:first)
    @planned_activity = PlannedActivity.find(id)
  end

  def to_param
    Hashids.new("DMMan",4).encode(id)
  end

  private

    def planned_activity_params
      params.require(:planned_activity).permit(:text)
    end
end
