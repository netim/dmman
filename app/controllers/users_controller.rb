class UsersController < ApplicationController
  def new
       @user = User.new
  end

  def show
    @user = current_user
    @planned_activities = @user.planned_activities.page params[:page]
    @performed_activities = @user.performed_activities.page params[:page]
  end
end
