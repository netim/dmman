class PerformedActivitiesController < ApplicationController
  # Utilização da gem hashids para obfuscar os IDs das atividades dos usuários
  # fonte: https://stackoverflow.com/questions/5496935/hash-instead-of-id
  
  def new
    @performed_activity = current_user.performed_activities.build
  end

  def create
    @performed_activity = current_user.performed_activities.build(performed_activity_params)
    if @performed_activity.save
      flash[:success] = "Atividades realizadas salvas!"
      redirect_to root_url
    else
      render 'new'
    end
  end

  def destroy
  end

  def show
    id = Hashids.new("DMMan",4).decode(params[:id]).try(:first)
    @performed_activity = PerformedActivity.find(id)
  end

  def to_param
    Hashids.new("DMMan",4).encode(id)
  end

  private

    def performed_activity_params
      params.require(:performed_activity).permit(:text)
    end
end
