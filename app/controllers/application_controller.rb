class ApplicationController < ActionController::Base
  # Requer autenticação do usuário
  before_action :authenticate_user!
end
