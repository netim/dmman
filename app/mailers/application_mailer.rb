class ApplicationMailer < ActionMailer::Base
  # Inclusão necessária para permitir o uso de url_helpers nos e-mails
  # fonte: https://stackoverflow.com/questions/17775264/rails-url-helpers-not-working-in-mailers
  include Rails.application.routes.url_helpers
  default from: 'admin@doe.com'
  layout 'mailer'
end
