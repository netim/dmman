class PerformedMailer < ApplicationMailer
  def performed_email
    @user = params[:user]
    @url = performed_activities_url
    time = Time.now
    mail(bcc: @user.email, subject: "Atividades realizadas #{time.day}/#{time.month}/#{time.year}")
  end
end
