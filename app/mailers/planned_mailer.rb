class PlannedMailer < ApplicationMailer
  def planned_email
    @user = params[:user]
    @url = planned_activities_url
    time = Time.now
    mail(bcc: @user.email, subject: "Atividades planejadas #{time.day}/#{time.month}/#{time.year}")
  end
end
