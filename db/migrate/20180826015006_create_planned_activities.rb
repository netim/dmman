class CreatePlannedActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :planned_activities do |t|
      #t.integer :user_id
      t.text :text
      t.references :user, foreign_key: true

      t.timestamps
    end
    #add_index :planned_activities, [:user_id, :created_at]
    #add_foreign_key :users, :user
  end
end
