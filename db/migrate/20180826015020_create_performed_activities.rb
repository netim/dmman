class CreatePerformedActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :performed_activities do |t|
      #t.integer :user_id
      t.text :text
      t.references :user, foreign_key: true

      t.timestamps
    end
    #add_index :performed_activities, [:user_id, :created_at]
    #add_foreign_key :users, :user
  end
end
