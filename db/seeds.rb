# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name:  "John Doe",
             email: "john@doe.com",
             password:              "123456",
             password_confirmation: "123456")

User.create!(name:  "Jane Doe",
             email: "jane@doe.com",
             password:              "456789",
             password_confirmation: "456789")

User.create!(name:  "Jackie Doe",
             email: "jackie@doe.com",
             password:              "456789",
             password_confirmation: "456789")

User.create!(name:  "Bruce Doe",
             email: "bruce@doe.com",
             password:              "456789",
             password_confirmation: "456789")

User.create!(name:  "Will Doe",
             email: "will@doe.com",
             password:              "456789",
             password_confirmation: "456789")

TeamName.create!(name: "Web")

TeamName.create!(name: "Mobile")

TeamName.create!(name: "IoT")

Team.create!(team_name_id: 1,
            user_id: 2)

Team.create!(team_name_id: 1,
            user_id: 3)

Team.create!(team_name_id: 2,
            user_id: 3)

Team.create!(team_name_id: 2,
            user_id: 4)

Team.create!(team_name_id: 3,
            user_id: 4)

Team.create!(team_name_id: 3,
           user_id: 5)
AdminUser.create!(email: 'admin@doe.com', password: 'password', password_confirmation: 'password') if Rails.env.development?
