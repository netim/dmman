# Gerenciador de Daily Meetings (DMMan)

Gerenciador de Daily Meetings (*DMMan*) é uma aplicação feita em Ruby on Rails 5.2 para atividades de equipes de TI.

## Requisitos

A aplicação foi desenvolvida em Ruby 2.5.1 e Rails 5.2.1. Em distribuições Linux, Ruby pode ser gerenciado via [Ruby Version Manager (RVM)](http://rvm.io/).

## Instalação

Após clonar ou baixar esse repositório, instale as *gems* necessárias:
```
bundle
```
Migre o banco de dados:
```
rails db:migrate
```
Opcional: adicione dados ao banco:
```
rails db:seed
```
Rode o servidor do Rails:
```
rails s
```
A aplicação pode ser acessada via [0.0.0.0:3000](http://0.0.0.0:3000/). O painel de administração do ActiveAdmin pode ser acessado via [0.0.0.0:3000/admin](http://0.0.0.0:3000/admin/) com email *admin@doe.com* e senha *password*.
Opcional: instale a *gem* **Mailcatcher** para visualizar os emails enviados:
```
gem install mailcatcher
```
Depois execute o comando para o mailcatcher rodar como *daemon*:
```
mailcatcher
```
O visualizador do Mailcatcher pode ser acessado via [localhost:1080](http://localhost:1080/). Por padrão, o Mailcatcher usa porta *1080* para receber o email. Para alterá-la, mude o valor de *port* na string *config.action_mailer.smtp_settings* do arquivo *config/environments/development.rb*.

## Utilização

A página inicial do *DMMan* mostra as tabelas das atividades  planejadas e realizadas do dia e links para adição de novas. Às 8h, a aplicação envia um email com o link para o formulário das atividades planejadas do dia. E às 16h, das atividades realizadas.

## Bibliotecas

* Puma como servidor;
* Bootstrap para estilos;
* Kaminari para paginação;
* ActiveAdmin para administração de usuários;
* Devise para autenticação e autorização;
* Simple form personalização de formulário;
* Hahids para obfuscação dos ids das atividades;
* Haml para simplificação do código HTML nas páginas criadas manualmente;
* Rufus scheduler para envio programado de emails;
* Pg para banco Postgresql (usuário *postgres* e senha *123456*). Tutoriais podem ser encontrados [aqui](https://www.digitalocean.com/community/tutorials/como-instalar-e-utilizar-o-postgresql-no-ubuntu-16-04-pt) e [aqui](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-ruby-on-rails-application-on-ubuntu-14-04).

## Agradecimentos

* Livro Ruby on Rails Tutorial (Rails 5) do Michael Hartl;
* Aos usuários do StackOverflow;
* Documentação da *gems*;
* W3Schools.
